//
//  QuotesViewModel.swift
//  QuotesIOS
//
//  Created by MAC on 25.12.2021.
//

import Foundation
import Combine

final class QuotesViewModel {

    // MARK: - Properties
    let coreDataBase: QuotesCoreDataBase
    var showAddQuoteScreen: ((QuoteModel?) -> Void)?

    // MARK: - Init
    init(coreDataBase: QuotesCoreDataBase) {
        self.coreDataBase = coreDataBase
    }

    // MARK: - Public functions
    func returnQoutes() -> [QuoteModel] {
        return coreDataBase.quotes
    }
}
