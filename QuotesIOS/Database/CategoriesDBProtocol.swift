//
//  CategoriesDBProtocol.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation
import Combine

protocol CategoriesDBProtocol {
    var categories: [CategoryModel] { get }
    func save(usingModel model: CategoryModel) -> Future<String, DatabaseError>
    var databaseResponseSubject: PassthroughSubject<DatabaseResponse, Never> { get }
}
