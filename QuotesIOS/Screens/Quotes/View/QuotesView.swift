//
//  QuotesView.swift
//  QuotesIOS
//
//  Created by MAC on 25.12.2021.
//

import UIKit
import Combine

final class QuotesView: UIView {

    // MARK: - Outlets
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(QuoteCell.self)
        tableView.backgroundColor = .white
        return tableView
    }()
    private(set) lazy var showAddQuoteButton: UIBarButtonItem = {
        let regularTextAppearance: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.purple,
            .font: UIFont(name: "MarkerFelt-Thine", size: 16) ?? UIFont.systemFont(ofSize: 16)
        ]
        let button = UIBarButtonItem(title: "Add Quote", style: .plain, target: self, action: nil)
        button.setTitleTextAttributes(regularTextAppearance, for: .normal)
        button.setTitleTextAttributes(regularTextAppearance, for: .highlighted)
        return button
    }()

    // MARK: - Properties
    private var viewModel: QuotesViewModel
    private var cancellables = Set<AnyCancellable>()

    // MARK: - Init
    init(viewModel: QuotesViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }

    // MARK: - Public API
    func tableViewReloadData() {
        tableView.reloadData()
    }

    func beginTableViewUpdates() {
        tableView.beginUpdates()
    }

    func endTableViewUpdates() {
        tableView.endUpdates()
    }

    func insertRowsTableView(indexPath: [IndexPath]) {
        tableView.insertRows(at: indexPath, with: .fade)
    }

    func deleteRowsTableView(indexPath: [IndexPath]) {
        tableView.deleteRows(at: indexPath, with: .fade)
    }
}

// MARK: - UITableViewDelegate
extension QuotesView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let quote = viewModel.returnQoutes()[indexPath.row]
        self.viewModel.showAddQuoteScreen?(quote)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let quote = viewModel.returnQoutes()[indexPath.row]
        if editingStyle == .delete {
            viewModel.coreDataBase.delete(quoteModel: quote)
        }
    }
}

// MARK: - UITableViewDataSource
extension QuotesView: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return viewModel.returnQoutes().count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: QuoteCell = tableView.cell(forRowAt: indexPath) else {
            return UITableViewCell()
        }
        let quote = viewModel.returnQoutes()[indexPath.row]
        cell.configureCell(quote: quote)
        return cell
    }
}

// MARK: - ConfigureUI
private extension QuotesView {

    func configureUI() {
        backgroundColor = .white
        addSubview(tableView)

        tableView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(0)
            $0.leading.equalTo(self).offset(0)
            $0.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self.safeAreaLayoutGuide).offset(0)
        }
    }
}
