//
//  UITableViewCell.swift
//  QuotesIOS
//
//  Created by MAC on 17.12.2021.
//

import UIKit

final class QuoteCell: UITableViewCell {

    // MARK: - Properties
    private let quoteTextLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()
    private let authorLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    private let commonView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        return view
    }()
    private let categoryLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()

    // MARK: - Initializers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - ConfigureCell
    func configureCell(quote: QuoteModel) {
        quoteTextLabel.text = quote.content
        authorLabel.text = quote.author
        commonView.backgroundColor = UIColor(hexString: quote.category.legend)
        categoryLabel.text = "Category: " + quote.category.name
    }
}

// MARK: - Configure UI
extension QuoteCell {

    func configureUI() {
        contentView.addSubview(commonView)
        commonView.addSubview(quoteTextLabel)
        commonView.addSubview(authorLabel)
        commonView.addSubview(categoryLabel)

        commonView.snp.makeConstraints {
            $0.top.leading.equalTo(contentView).offset(5)
            $0.bottom.trailing.equalTo(contentView).offset(-5)
        }

        quoteTextLabel.snp.makeConstraints {
            $0.top.equalTo(commonView).offset(10)
            $0.leading.equalTo(commonView).offset(20)
            $0.trailing.equalTo(commonView).offset(-20)
        }

        categoryLabel.snp.makeConstraints {
            $0.leading.equalTo(commonView).offset(20)
            $0.centerY.equalTo(authorLabel.snp.centerY)
        }

        authorLabel.snp.makeConstraints {
            $0.top.equalTo(quoteTextLabel.snp.bottom).offset(5)
            $0.trailing.equalTo(commonView).offset(-20)
            $0.bottom.equalTo(commonView).offset(-10)
        }
    }
}
