//
//  CategoryModel.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation

struct CategoryModel {

    let id: String
    let name: String
    let legend: String

    init(name: String, legend: String) {
        self.id = UUID().uuidString
        self.name = name
        self.legend = legend
    }
}

extension CategoryModel {

    init(usingEntity entity: Category?) {
        self.id = entity?.id ?? UUID().uuidString
        self.name = entity?.name ?? "UNKNOWN"
        self.legend = entity?.legend ?? "000000"
    }
}
