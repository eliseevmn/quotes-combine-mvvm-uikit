//
//  CategoriesViewController.swift
//  QuotesIOS
//
//  Created by MAC on 20.12.2021.
//

import UIKit
import Combine

final class CategoriesViewController: UIViewController {

    // MARK: - Outlets
    private lazy var categoriesView = self.view as? CategoriesView

    // MARK: - Properties
    let viewModel: CategoriesViewModel
    private var cancellables = Set<AnyCancellable>()

    // MARK: - Init
    init(viewModel: CategoriesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = CategoriesView(viewModel: viewModel)
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitle(title: "Categories")
        configureUI()
        setupBinding()
    }
}

private extension CategoriesViewController {

    func setupBinding() {
        viewModel.coreDataBase.databaseResponseSubject
            .sink(receiveValue: { [weak self] databasResponse in
                guard let self = self else { return }
                switch databasResponse {
                    case .beginUpdates:
                        self.categoriesView?.beginTableViewUpdates()
                        break
                    case .endUpdates:
                        self.categoriesView?.endTableViewUpdates()
                        break
                    case .insert(indexPpath: let indexPpath):
                        self.categoriesView?.insertRowsTableView(indexPath: [indexPpath])
                        break
                    case .delete(indexPpath: let indexPath):
                        print(indexPath)
                        break
                }
            })
            .store(in: &cancellables)

        categoriesView?.showAddQuoteButton.tapPublisher
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.showAddCategoryScreen?()
            }
            .store(in: &cancellables)
    }
}

private extension CategoriesViewController {

    func configureUI() {
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = categoriesView?.showAddQuoteButton
    }
}
