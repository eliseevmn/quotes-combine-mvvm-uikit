//
//  CategoryCell.swift
//  QuotesIOS
//
//  Created by MAC on 20.12.2021.
//

import UIKit

final class CategoryCell: UITableViewCell {

    private let hexColorView: UIView = {
        let view = UIView()
        return view
    }()
    private let categoryNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureCell(category: CategoryModel) {
        hexColorView.backgroundColor = UIColor(hexString: category.legend)
        categoryNameLabel.text = category.name
    }
}

// MARK: - Configure UI
extension CategoryCell {

    func configureUI() {
        contentView.addSubview(hexColorView)
        contentView.addSubview(categoryNameLabel)

        hexColorView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(2)
            $0.leading.equalTo(contentView).offset(20)
            $0.bottom.equalTo(contentView).offset(-2)
            $0.width.equalTo(30)
            $0.height.equalTo(40)
        }

        categoryNameLabel.snp.makeConstraints {
            $0.leading.equalTo(hexColorView.snp.trailing).offset(10)
            $0.centerY.equalTo(contentView).offset(0)
            $0.trailing.equalTo(contentView).offset(-20)
        }
    }
}
