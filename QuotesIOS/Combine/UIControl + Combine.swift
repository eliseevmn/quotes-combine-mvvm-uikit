//
//  UIControl + Combine.swift
//  QuotesIOS
//
//  Created by MAC on 22.12.2021.
//

import UIKit
import Combine

protocol CombineCompatible {

}

extension UIControl: CombineCompatible {

}

extension UIControl {

    final class Subscription<SubscriberType: Subscriber, Control: UIControl>: Combine.Subscription where SubscriberType.Input == Control {

        private var subscriber: SubscriberType?
        private let control: Control

        init(subscriber: SubscriberType, control: Control, event: Control.Event) {
            self.subscriber = subscriber
            self.control = control
            self.control.addTarget(self, action: #selector(eventHandler), for: event)
        }

        func request(_ demand: Subscribers.Demand) { }

        func cancel() {
            subscriber = nil
        }

        @objc func eventHandler() {
            _ = subscriber?.receive(control)
        }
    }

    struct Publisher<Output: UIControl>: Combine.Publisher {

        typealias Output = Output
        typealias Failure = Never

        private let control: Output
        private let event: Output.Event

        init(control: Output, event: Output.Event) {
            self.control = control
            self.event = event
        }

        func receive<S>(subscriber: S) where S : Subscriber, Never == S.Failure, Output == S.Input {
            let subscription = Subscription(subscriber: subscriber,
                                            control: control,
                                            event: event)
            subscriber.receive(subscription: subscription)
        }
    }
}

extension CombineCompatible where Self: UIControl {

    func publisher(for event: UIControl.Event) -> UIControl.Publisher<UIControl> {
        .init(control: self, event: event)
    }

    var tapPublisher: AnyPublisher<Void, Never> {
        self.publisher(for: .touchUpInside).map({ _ in () }).eraseToAnyPublisher()
    }
}
