//
//  AddQuoteCoordinator.swift
//  QuotesIOS
//
//  Created by MAC on 25.12.2021.
//

import UIKit

final class AddQuoteCoordinator: BaseCoordinator {

    // MARK: - Properties
    private let moduleFactory: ModuleFactoryProtocol
    private let router: Router

    // MARK: - Init
    init(router: Router, moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
        self.router = router
    }

    // MARK: - Navigation functions
    override func start() {
        showAddQuoteScreen()
    }

    private func showAddQuoteScreen() {

    }
}
