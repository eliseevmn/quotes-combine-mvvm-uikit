//
//  SceneDelegate.swift
//  QuotesIOS
//
//  Created by MAC on 17.12.2021.
//

import UIKit
import SnapKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    fileprivate var appCoordinator: Coordinatable?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        runUI()
    }
}

extension SceneDelegate {

    fileprivate func runUI() {

        let navigationController = UINavigationController()
        let router = Router(rootController: navigationController)
        let moduleFactory = ModuleFactory()
        let coordinatorFactory = CoordinatorFactory(moduleFactory: moduleFactory)
        self.appCoordinator = coordinatorFactory.makeQuotesCoordinator(with: router)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        self.appCoordinator?.start()
    }

}

