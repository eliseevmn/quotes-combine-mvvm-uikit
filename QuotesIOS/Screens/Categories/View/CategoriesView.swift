//
//  CategoriesView.swift
//  QuotesIOS
//
//  Created by MAC on 29.12.2021.
//

import UIKit
import Combine

final class CategoriesView: UIView {

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(CategoryCell.self)
        return tableView
    }()

    private(set) lazy var showAddQuoteButton: UIBarButtonItem = {
        let regularTextAppearance: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.purple,
            .font: UIFont(name: "MarkerFelt-Thine", size: 16) ?? UIFont.systemFont(ofSize: 16)
        ]
        let button = UIBarButtonItem(title: "Add Category", style: .plain, target: self, action: nil)
        button.setTitleTextAttributes(regularTextAppearance, for: .normal)
        button.setTitleTextAttributes(regularTextAppearance, for: .highlighted)
        return button
    }()

    // MARK: - Properties
    private let viewModel: CategoriesViewModel
    private var cancellable = Set<AnyCancellable>()

    // MARK: - init
    init(viewModel: CategoriesViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }

    // MARK: - Public API
    func beginTableViewUpdates() {
        tableView.beginUpdates()
    }

    func endTableViewUpdates() {
        tableView.endUpdates()
    }

    func insertRowsTableView(indexPath: [IndexPath]) {
        tableView.insertRows(at: indexPath, with: .fade)
    }
}
// MARK: - UITableViewDelegate
extension CategoriesView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.categorySelected(indexPath: indexPath)
        guard let categoryModel = viewModel.onCategorySelected else { return }
        self.viewModel.isFinishScreen?(categoryModel)
    }
}

// MARK: - UITableViewDataSource
extension CategoriesView: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return viewModel.returnCategories().count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: CategoryCell = tableView.cell(forRowAt: indexPath) else {
            return UITableViewCell()
        }

        let category = viewModel.returnCategories()[indexPath.row]
        cell.configureCell(category: category)
        return cell
    }
}

// MARK: - ConfigureUI
private extension CategoriesView {

    func configureUI() {
        backgroundColor = .white

        addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(0)
            $0.leading.equalTo(self).offset(0)
            $0.trailing.equalTo(self).offset(0)
            $0.bottom.equalTo(self).offset(0)
        }
    }
}


