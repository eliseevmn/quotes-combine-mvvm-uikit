//
//  QuotesCoordinator.swift
//  QuotesIOS
//
//  Created by MAC on 25.12.2021.
//

import UIKit
import Combine

final class QuotesCoordinator: BaseCoordinator {

    // MARK: - Private Properties
    private let coordinatorFactory: CoordinatorFactory
    private let moduleFactory: ModuleFactoryProtocol
    private let router: Router

    @Published var categoryModel: CategoryModel?
    var addQuoteScreen: AddQuoteViewController?

    // MARK: - Initialisers

    init(router: Router,
         coordinatorFactory: CoordinatorFactory,
         moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
        self.router = router
    }

    // MARK: - Public Methods

    override func start() {
        initializeTabBar()
    }

    // MARK: - Private Methods

    private func initializeTabBar() {
        let controller = moduleFactory.makeQuotesScreenModule()
        controller.viewModel.showAddQuoteScreen = { quoteModel in
            self.showAddQuoteScreen(quoteModel: quoteModel)
        }
        router.setRoot(controller, animated: false)
    }

    private func showAddQuoteScreen(quoteModel: QuoteModel?) {
        addQuoteScreen = moduleFactory.makeAddQuoteScreenModule()
        addQuoteScreen?.viewModel.selectedQuote.value = quoteModel
        
        addQuoteScreen?.viewModel.isFinishScreen = {
            self.router.pop(animated: true)
        }
        addQuoteScreen?.viewModel.showAddCategoryScreen = {
            self.showCategoriesScreen()
        }
        router.push(addQuoteScreen ?? UIViewController())
    }

    private func showCategoriesScreen() {
        let categoriesScreen = moduleFactory.makeCategoriesScreenModule()
        categoriesScreen.viewModel.showAddCategoryScreen = {
            self.showAddCategoryScreen()
        }
        categoriesScreen.viewModel.isFinishScreen = { [weak self] categoryModel in
            guard let self = self else { return }
            self.addQuoteScreen?.viewModel.selectedCategory.value = categoryModel
            self.router.pop(animated: true)
        }
        router.push(categoriesScreen)
    }

    private func showAddCategoryScreen() {
        let addCategoryScreen = moduleFactory.makeAddCategoryScreenModule()
        addCategoryScreen.viewModel.isFinishScreen = {
            self.router.pop(animated: true)
        }
        router.push(addCategoryScreen)
    }
}
