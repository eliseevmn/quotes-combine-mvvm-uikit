//
//  DIContainer.swift
//  QuotesIOS
//
//  Created by MAC on 23.12.2021.
//

import Foundation

final class DIContainer {

    // MARK: - Public Properties

    let session: URLSession
    let decoder: JSONDecoder

    let keychainWrapper: UserDefaults
    let quotesDataBase: QuotesCoreDataBase
    let categoriesDataBase: CategoriesCoreDatabase

//    let requestBuilder: RequestBuilder
//    let apiClient: APIClient
//    let tokenRepository: TokenRepository
//
//    lazy var userService = UserService(userAPIClient: apiClient, tokenRepository: tokenRepository)
//    lazy var authService = AuthService(authAPIClient: apiClient, tokenRepository: tokenRepository)
//    lazy var productService = ProductService(productAPIClient: apiClient)

    // MARK: - Initialisers

    init() {
        quotesDataBase = QuotesCoreDataBase()
        categoriesDataBase = CategoriesCoreDatabase()
        keychainWrapper = UserDefaults.standard
//        tokenRepository = TokenRepository(keychainWrapper: keychainWrapper)
//        requestBuilder = RequestBuilder()
        session = URLSession.shared
        decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
//        apiClient = APIClient(requestBuilder: requestBuilder, session: session, decoder: decoder)
    }
}
