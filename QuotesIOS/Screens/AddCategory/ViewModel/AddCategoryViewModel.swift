//
//  AddCategoryViewModel.swift
//  QuotesIOS
//
//  Created by MAC on 29.12.2021.
//

import Foundation
import Combine

final class AddCategoryViewModel {

    // MARK: - Properties
    var coreDataBase: CategoriesCoreDatabase
    var isFinishScreen: (() -> Void)?
    var selectedColor: CurrentValueSubject<String?, Never> = CurrentValueSubject("000000")

    // MARK: - Init
    init(coreDataBase: CategoriesCoreDatabase) {
        self.coreDataBase = coreDataBase
    }
}
