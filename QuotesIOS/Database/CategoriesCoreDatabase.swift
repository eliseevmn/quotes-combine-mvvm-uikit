//
//  CategoriesCoreDataDatabase.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation
import CoreData
import Combine

final class CategoriesCoreDatabase: NSObject {

    // MARK: - Properties
    private let context: NSManagedObjectContext = QuotesPersistenceContainer().context
    var databaseResponseSubject: PassthroughSubject<DatabaseResponse, Never> = PassthroughSubject()

    private lazy var controller: NSFetchedResultsController<Category> = {
        let fetchRequest: NSFetchRequest<Category> = Category.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                managedObjectContext: context,
                                                                sectionNameKeyPath: nil,
                                                                cacheName: nil)
        controller.delegate = self
        return controller
    }()

    // MARK: - Init
    override init() {
        super.init()

        do {
            try self.controller.performFetch()
        } catch let fetchError {
            print("PErform fetch failed: \(fetchError)")
        }
    }
}

// MARK: - CategoriesDBProtocol
extension CategoriesCoreDatabase: CategoriesDBProtocol {

    var categories: [CategoryModel] {
        guard let entities = controller.fetchedObjects else {
            return []
        }
        return entities.compactMap { CategoryModel(usingEntity: $0) }
    }

    func save(usingModel model: CategoryModel) -> Future<String, DatabaseError> {
        let category: Category = Category(context: context)
        category.id = model.id
        category.name = model.name
        category.legend = model.legend

        return Future { [context] promise in
            do {
                try context.save()

                promise(.success("Save category is succesful!!!"))
            } catch let saveError {
                print("Category Save failed with error: ",saveError)
                promise(.failure(.saveError))
            }
        }
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension CategoriesCoreDatabase: NSFetchedResultsControllerDelegate {

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        databaseResponseSubject.send(.beginUpdates)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        databaseResponseSubject.send(.endUpdates)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch(type) {
            case .insert:
                if let indexPath = newIndexPath {
                    databaseResponseSubject.send(.insert(indexPpath: indexPath))
                }
                break
            default:
                break
        }
    }
}
