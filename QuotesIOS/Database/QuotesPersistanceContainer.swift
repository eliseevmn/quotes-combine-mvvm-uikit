//
//  QuotesPersistanceContainer.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation
import CoreData

class QuotesPersistenceContainer {

    private static let persistenceContainer: NSPersistentContainer = {
        let persisteneContainer = NSPersistentContainer(name: "LetsDoQuotes")

        persisteneContainer.loadPersistentStores { description, error in
            if let error = error {
                print("Unable lo load the persistence store")
                print("\(error)")
            } else {
                print("Persistence store is loaded succesfully!")
            }
        }

        return persisteneContainer
    }()

    var context: NSManagedObjectContext {
        return Self.persistenceContainer.viewContext
    }
}
