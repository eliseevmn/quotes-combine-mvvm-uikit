//
//  Router.swift
//  QuotesIOS
//
//  Created by MAC on 23.12.2021.
//

import UIKit

protocol Routable {

    func present(_ viewController: UIViewController, animated: Bool)
    func push(_ viewController: UIViewController, animated: Bool)
    func pop(animated: Bool)
    func dismiss(animated: Bool)
    func setRoot(_ viewController: UIViewController, animated: Bool)
    func setRoot(_ viewController: UIViewController, animated: Bool, hideBar: Bool)
    func popToRoot(animated: Bool)
}

final class Router: Routable {

    private weak var rootNavController: UINavigationController?

    init(rootController: UINavigationController) {
        self.rootNavController = rootController
    }

    func present(_ viewController: UIViewController, animated: Bool = true) {
        rootNavController?.present(viewController, animated: animated, completion: nil)
    }

    func push(_ viewController: UIViewController, animated: Bool = true) {
        rootNavController?.pushViewController(viewController, animated: true)
    }

    func pop(animated: Bool) {
        rootNavController?.popViewController(animated: true)
    }

    func dismiss(animated: Bool) {
        rootNavController?.dismiss(animated: true, completion: nil)
    }

    func setRoot(_ viewController: UIViewController, animated: Bool) {
        setRoot(viewController, animated: animated, hideBar: false)
    }

    func setRoot(_ viewController: UIViewController, animated: Bool, hideBar: Bool) {
        rootNavController?.setViewControllers([viewController], animated: animated)
        rootNavController?.isNavigationBarHidden = hideBar
    }

    func popToRoot(animated: Bool) {
        rootNavController?.popToRootViewController(animated: animated)
    }
}
