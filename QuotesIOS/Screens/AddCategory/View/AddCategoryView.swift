//
//  AddCategoryView.swift
//  QuotesIOS
//
//  Created by MAC on 31.12.2021.
//

import UIKit

final class AddCategoryView: UIView {

    // MARK: - Outlets
    private lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.text = "Category"
        label.textColor = .black
        return label
    }()

    private(set) lazy var categoryTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "What's the category?"
        return textField
    }()

    private(set) lazy var hexCodeLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private(set) lazy var legendView: UIView = {
        let view = UIView()
        return view
    }()

    private(set) lazy var chooseCategoryColorButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Choose Color", for: .normal)
        button.backgroundColor = .purple
        button.layer.cornerRadius = 10
        button.setTitleColor(.white, for: .normal)
        return button
    }()


    private(set) lazy var saveCategoryButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Add", for: .normal)
        button.backgroundColor = .purple
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 10
        return button
    }()

    private(set) lazy var cancelAddCategoryButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        return button
    }()

    // MARK: - Properties
    private let viewModel: AddCategoryViewModel

    // MARK: - Init
    init(viewModel: AddCategoryViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configure UI
extension AddCategoryView {

    func configureUI() {
        backgroundColor = .white

        addButtonsStackView()

        let colorStackView = UIStackView(arrangedSubviews: [hexCodeLabel,
                                                            legendView])
        colorStackView.axis = .vertical
        let horizontantalStackView = UIStackView(arrangedSubviews: [colorStackView,
                                                                    chooseCategoryColorButton])
        horizontantalStackView.axis = .horizontal
        horizontantalStackView.spacing = 20


        let verticalSTackView = UIStackView(arrangedSubviews: [categoryLabel,
                                                               categoryTextField,
                                                               horizontantalStackView])
        self.addSubview(verticalSTackView)
        verticalSTackView.axis = .vertical
        verticalSTackView.spacing = 20
        verticalSTackView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(25)
            $0.leading.equalTo(self).offset(20)
            $0.trailing.equalTo(self).offset(-20)
        }

        legendView.snp.makeConstraints {
            $0.height.equalTo(1)
        }

        categoryTextField.snp.makeConstraints {
            $0.height.equalTo(50)
        }

        hexCodeLabel.snp.makeConstraints {
            $0.height.equalTo(40)
        }

        chooseCategoryColorButton.snp.makeConstraints {
            $0.width.equalTo(120)
        }
    }

    func addButtonsStackView() {
        let buttonsStackView = UIStackView(arrangedSubviews: [saveCategoryButton,
                                                              cancelAddCategoryButton])
        self.addSubview(buttonsStackView)
        buttonsStackView.spacing = 10
        buttonsStackView.distribution = .fillEqually
        buttonsStackView.axis = .horizontal

        buttonsStackView.snp.makeConstraints {
            $0.bottom.equalTo(self.safeAreaLayoutGuide).offset(-10)
            $0.leading.equalTo(self).offset(20)
            $0.trailing.equalTo(self).offset(-20)
            $0.height.equalTo(50)
        }
    }
}

