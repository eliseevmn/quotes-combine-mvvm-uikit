//
//  UIBarButtonItem + COmbine.swift
//  QuotesIOS
//
//  Created by MAC on 22.12.2021.
//

import UIKit
import Combine

extension UIBarButtonItem: CombineCompatible { }

extension UIBarButtonItem {

    class Subscription<SubscriberType: Subscriber, Input: UIBarButtonItem>: Combine.Subscription where SubscriberType.Input == Input {

        private var subscriber: SubscriberType?
        private let input: Input

        init(subscriber: SubscriberType, input: Input) {
            self.subscriber = subscriber
            self.input = input
            self.input.target = self
            self.input.action = #selector(eventHandler)
        }

        func request(_ demand: Subscribers.Demand) {}

        func cancel() {
            subscriber = nil
        }

        @objc func eventHandler() {
            _ = subscriber?.receive(self.input)
        }
    }

    struct Publisher<Output: UIBarButtonItem>: Combine.Publisher {

        typealias Output = Output
        typealias Failure = Never

        private let output: Output

        init(output: Output) {
            self.output = output
        }

        func receive<S>(subscriber: S) where S : Subscriber, Never == S.Failure, Output == S.Input {
            let subscription = Subscription(subscriber: subscriber,
                                            input: output)
            subscriber.receive(subscription: subscription)
        }
    }
}

extension CombineCompatible where Self: UIBarButtonItem {

    var publisher: UIBarButtonItem.Publisher<UIBarButtonItem> {
        .init(output: self)
    }

    var tapPublisher: AnyPublisher<Void, Never> {
        Publisher(output: self).map({ _ in () }).eraseToAnyPublisher()
    }
}

