//
//  UIViewController+Extensions.swift
//  QuotesIOS
//
//  Created by MAC on 20.12.2021.
//

import UIKit

extension UIViewController {

    func setupTitle(title: String) {

        let appearance = UINavigationBarAppearance()

        let largeTitleFont = UIFont.preferredFont(forTextStyle: .largeTitle)
        let largeFontSize = largeTitleFont.pointSize

        let titleFont = UIFont.preferredFont(forTextStyle: .title1)
        let fontSize = titleFont.pointSize

        appearance.largeTitleTextAttributes = [
            .foregroundColor: UIColor.purple,
            .font: UIFont(name: "MarkerFelt-Thin", size: largeFontSize)!
        ]
        appearance.titleTextAttributes = [
            .foregroundColor: UIColor.purple,
            .font: UIFont(name: "MarkerFelt-Thin", size: fontSize)!
        ]

        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.standardAppearance = appearance
        self.navigationItem.scrollEdgeAppearance = appearance

        self.navigationController?.navigationBar.tintColor = UIColor.purple
        self.title = title
    }
}
