//
//  AddQuoteView.swift
//  QuotesIOS
//
//  Created by MAC on 28.12.2021.
//

import UIKit

final class AddQuoteView: UIView {

    // MARK: - Outlets
    private lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.text = "Author"
        return label
    }()

    lazy var authorTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Who's the author?"
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.systemGray4.cgColor
        textField.layer.cornerRadius = 10
        return textField
    }()

    private lazy var chooseCategoryLabel: UILabel = {
        let label = UILabel()
        label.text = "Choose Category"
        return label
    }()

    private(set) lazy var chooseCategoryButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("None", for: .normal)
        return button
    }()

    private lazy var quoteLabel: UILabel = {
        let label = UILabel()
        label.text = "Quote"
        return label
    }()

    private(set) lazy var textTextView: UITextView = {
        let textView = UITextView()
        textView.text = "What's your quote?"
        textView.backgroundColor = .systemGray4
        textView.layer.cornerRadius = 10
        return textView
    }()

    private(set) lazy var addQuoteButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Add", for: .normal)
        button.backgroundColor = .red
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 10
        return button
    }()

    private(set) lazy var cancelAddQuoteButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        return button
    }()

    // MARK: - Properties
    private let viewModel: AddQuoteViewModel

    // MARK: - init
    init(viewModel: AddQuoteViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public API
    func updateChoosenButton(categoryModel: CategoryModel? = nil) {
        let customButtonTitle = NSMutableAttributedString(string: categoryModel?.name ?? "None", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14) ])
        chooseCategoryButton.setAttributedTitle(customButtonTitle, for: .normal)
        chooseCategoryButton.setAttributedTitle(customButtonTitle, for: .focused)
        chooseCategoryButton.setAttributedTitle(customButtonTitle, for: .highlighted)
        if let legend = categoryModel?.legend {
            chooseCategoryButton.tintColor = UIColor(hexString: legend)
        }
    }

    func updateDataQuote(quoteModel: QuoteModel?) {
        authorTextField.text = quoteModel?.author
        textTextView.text = quoteModel?.content
    }
}

// MARK: - Configure UI
private extension AddQuoteView {

    func configureUI() {
        self.backgroundColor = .white

        addVerticalStackView()
        addButtonsStackView()

        addQuoteButton.snp.makeConstraints {
            $0.height.equalTo(50)
        }

        cancelAddQuoteButton.snp.makeConstraints {
            $0.height.equalTo(50)
        }

        textTextView.snp.makeConstraints {
            $0.height.equalTo(100)
        }

        authorTextField.snp.makeConstraints {
            $0.height.equalTo(50)
        }
    }

    func addVerticalStackView() {
        let chooseCategoryStackView = UIStackView(arrangedSubviews: [chooseCategoryLabel,
                                                                     chooseCategoryButton])
        chooseCategoryStackView.axis = .horizontal
        chooseCategoryStackView.distribution = .fillEqually
        chooseCategoryStackView.spacing = 10

        let verticalStackView = UIStackView(arrangedSubviews: [authorLabel,
                                                               authorTextField,
                                                               chooseCategoryStackView,
                                                               quoteLabel,
                                                               textTextView])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 20
        self.addSubview(verticalStackView)
        verticalStackView.snp.makeConstraints {
            $0.top.equalTo(self.safeAreaLayoutGuide).offset(25)
            $0.leading.equalTo(self).offset(20)
            $0.trailing.equalTo(self).offset(-20)
        }

    }

    func addButtonsStackView() {
        let buttonsStackView = UIStackView(arrangedSubviews: [addQuoteButton,
                                                              cancelAddQuoteButton])
        self.addSubview(buttonsStackView)
        buttonsStackView.spacing = 10
        buttonsStackView.distribution = .fillEqually
        buttonsStackView.axis = .horizontal

        buttonsStackView.snp.makeConstraints {
            $0.bottom.equalTo(self.safeAreaLayoutGuide).offset(-10)
            $0.leading.equalTo(self).offset(20)
            $0.trailing.equalTo(self).offset(-20)
        }
    }
}
