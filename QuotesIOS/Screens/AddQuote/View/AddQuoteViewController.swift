//
//  AddQuoteViewController.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import UIKit
import Combine

final class AddQuoteViewController: UIViewController {

    // MARK: - Outlets
    private lazy var addQuoteView = self.view as? AddQuoteView

    // MARK: - Properties
    private var cancellables = Set<AnyCancellable>()
    let viewModel: AddQuoteViewModel
    private lazy var coreDataBase = viewModel.coreDataBase

    // MARK: - Init
    init(viewModel: AddQuoteViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = AddQuoteView(viewModel: viewModel)
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupBinding()
    }
}

extension AddQuoteViewController {

    func setupBinding() {
        guard let addQuoteView = addQuoteView else { return }
        let combines = Publishers.CombineLatest3(addQuoteView.authorTextField.textPublisher,
                                                 addQuoteView.textTextView.textPublisher,
                                                 viewModel.selectedCategory)


        if viewModel.selectedQuote.value != nil {
            addQuoteView.updateDataQuote(quoteModel: viewModel.selectedQuote.value)
            viewModel.bindingCategoryModel()

            addQuoteView.addQuoteButton.tapPublisher
                .withLatestFrom(combines)
                .flatMap({ [coreDataBase] author, content, category in
                    coreDataBase.update(quoteModel: QuoteModel(
                        id: self.viewModel.selectedQuote.value?.id ?? "222",
                        author: author,
                        content: content,
                        category: category ?? CategoryModel(name: "", legend: "")))
                })
                .sink { error in
                    print(error)
                } receiveValue: { [weak self] _ in
                    guard let self = self else { return }
                    self.viewModel.isFinishScreen?()
                }
                .store(in: &cancellables)


        } else {
            addQuoteView.addQuoteButton.tapPublisher
                .withLatestFrom(combines)
                .filter({ $0.2 != nil })
                .map { author, content, category in
                    return (author, content, category!)
                }
                .map { author, content, category in
                    QuoteModel(id: self.addRandomId(),
                               author: author,
                               content: content,
                               category: category)
                }
                .flatMap { [coreDataBase] in
                    coreDataBase.save(usingModel: $0)
                }
                .sink { error in
                    print(error)
                } receiveValue: { [weak self] text in
                    guard let self = self else { return }
                    self.viewModel.isFinishScreen?()
                }
                .store(in: &cancellables)
        }


        addQuoteView.cancelAddQuoteButton.tapPublisher
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.isFinishScreen?()
            }
            .store(in: &cancellables)

        addQuoteView.chooseCategoryButton.tapPublisher
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.showAddCategoryScreen?()
            }
            .store(in: &cancellables)

        viewModel.selectedCategory
            .sink(receiveValue: { [addQuoteView] model in
                addQuoteView.updateChoosenButton(categoryModel: model)
                addQuoteView.chooseCategoryButton.tintColor = UIColor(hexString: model?.legend)
            })
            .store(in: &cancellables)
    }


    func addRandomId() -> String {
        let randomInt = Int.random(in: 1..<5)
        return "\(randomInt)"
    }
}

// MARK: - ConfigureUI
private extension AddQuoteViewController {

    func configureUI() {
        setupTitle(title: "Add Quote")
    }
}
