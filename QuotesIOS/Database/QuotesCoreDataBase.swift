//
//  CoreDataBase.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation
import CoreData
import Combine

final class QuotesCoreDataBase: NSObject {

    // MARK: - Properties
    let context: NSManagedObjectContext = QuotesPersistenceContainer().context
    var databaseResponseSubject: PassthroughSubject<DatabaseResponse, Never> = PassthroughSubject()


    private lazy var fetchResultsController: NSFetchedResultsController<Quote> = {
        let fetchRequest: NSFetchRequest<Quote> = Quote.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
        let fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                managedObjectContext: context,
                                                                sectionNameKeyPath: nil,
                                                                cacheName: nil)
        fetchResultsController.delegate = self
        return fetchResultsController
    }()

    // MARK: - Initializer
    override init() {
        super.init()

        do {
            try self.fetchResultsController.performFetch()
        } catch let fetchError {
            print("PErform fetch failed: \(fetchError)")
        }
    }
}

// MARK: - QuotesDBProtocol
extension QuotesCoreDataBase: QuotesDBProtocol {

    var quotes: [QuoteModel] {
        guard let entities = fetchResultsController.fetchedObjects else {
            return []
        }
        return entities.compactMap { QuoteModel(usingEntity: $0) }
    }

    func save(usingModel model: QuoteModel) -> Future<String, DatabaseError> {
        let quote = Quote(context: context)
        quote.author = model.author
        quote.content = model.content
        quote.timestamp = model.timestamp
        quote.id = model.id

        let categoryFetch = Category.fetchRequest()

        return Future { [context] promise in
            do {
                if let category = try context.fetch(categoryFetch).filter ({ $0.id == model.category.id }).first {
                    quote.category = category
                }
                try context.save()

                promise(.success("Save quote is succesful!!!"))
            } catch let saveError {
                print("Save failed with error: ",saveError)
                promise(.failure(.saveError))
            }
        }
    }

    func update(quoteModel: QuoteModel) -> Future<String, DatabaseError> {

        var quote: Quote?
        let fetchRequest: NSFetchRequest<Quote> = Quote.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %@", quoteModel.id)

        let categoryFetch = Category.fetchRequest()

        return Future { [context] promise in
            do {

                let results = try context.fetch(fetchRequest)

                if !results.isEmpty {
                    quote = results.first!
                    quote?.content = quoteModel.content
                    quote?.author = quoteModel.author

                    if let category = try context.fetch(categoryFetch).filter ({ $0.id == quoteModel.category.id }).first {
                        quote?.category = category
                    }
                }

                try context.save()
                promise(.success("Save quote is succesful!!!"))
            } catch let saveError {
                print("Save failed with error: ",saveError)
                promise(.failure(.saveError))
            }
        }
    }

    func delete(quoteModel: QuoteModel) {
//        let object = fetchResultsController.object(at: indexPath)
//        context.delete(object)
//        do {
//            try context.save()
//        } catch let saveError {
//            print("Save failed with error: ",saveError)
//        }

        let fetchRequest: NSFetchRequest<Quote> = Quote.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %@", quoteModel.id)
//        let categoryFetch = Category.fetchRequest()

//        return Future { [context] promise in
            do {
                let results = try context.fetch(fetchRequest)
                guard let objc = results.first else { return }
                context.delete(objc)

                try context.save()
//                promise(.success("Save quote is succesful!!!"))
            } catch let deleteError {
                print("Save failed with error: ", deleteError)
//                promise(.failure(.deleteError))
            }
//        }
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension QuotesCoreDataBase: NSFetchedResultsControllerDelegate {

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        databaseResponseSubject.send(.beginUpdates)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        databaseResponseSubject.send(.endUpdates)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            case .insert:
                if let indexPath = newIndexPath {
                    databaseResponseSubject.send(.insert(indexPpath: indexPath))
                }
                break
            case .delete:
                if let indexPath = newIndexPath {
                    databaseResponseSubject.send(.delete(indexPpath: indexPath))
                }
            default:
                break
        }
    }
}
