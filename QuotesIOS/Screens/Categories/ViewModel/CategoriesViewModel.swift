//
//  CategoriesViewModel.swift
//  QuotesIOS
//
//  Created by MAC on 29.12.2021.
//

import Foundation
import Combine

final class CategoriesViewModel {

    // MARK: - Properties
    let coreDataBase: CategoriesCoreDatabase
    @Published var onCategorySelected: CategoryModel?
    var isFinishScreen: ((CategoryModel) -> Void)?
    var showAddCategoryScreen: (() -> Void)?

    // MARK: - Init
    init(coreDataBase: CategoriesCoreDatabase) {
        self.coreDataBase = coreDataBase
    }

    // MARK: - Public functions
    func returnCategories() -> [CategoryModel] {
        return coreDataBase.categories
    }

    func categorySelected(indexPath: IndexPath) {
        self.onCategorySelected = coreDataBase.categories[indexPath.row]
    }
}
