//
//  ViewController.swift
//  QuotesIOS
//
//  Created by MAC on 17.12.2021.
//

import UIKit
import Combine

final class QuotesViewController: UIViewController {

    // MARK: - Outlets
    private lazy var quotesView = self.view as? QuotesView

    // MARK: - Properties
    let viewModel: QuotesViewModel
    private var cancellables = Set<AnyCancellable>()

    // MARK: - Init
    init(viewModel: QuotesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        let view = QuotesView(viewModel: viewModel)
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        quotesView?.tableViewReloadData()
    }
}

// MARK: - Bindings
private extension QuotesViewController {

    func setupBindings() {
        quotesView?.showAddQuoteButton.tapPublisher
            .sink(receiveValue: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.showAddQuoteScreen?(nil)
            })
            .store(in: &cancellables)

        viewModel.coreDataBase.databaseResponseSubject
            .sink(receiveValue: { [weak self] databasResponse in
                guard let self = self else { return }
                switch databasResponse {
                    case .beginUpdates:
                        self.quotesView?.beginTableViewUpdates()
                        break
                    case .endUpdates:
                        self.quotesView?.endTableViewUpdates()
                        break
                    case .insert(indexPpath: let indexPpath):
                        self.quotesView?.insertRowsTableView(indexPath: [indexPpath])
                        break
                    case .delete(indexPpath: let indexPath):
                        self.quotesView?.deleteRowsTableView(indexPath: [indexPath])
                        break
                }
            })
            .store(in: &cancellables)
    }
}

// MARK: - ConfigureUI
private extension QuotesViewController {

    func configureUI() {
        setupTitle(title: "Quotes")
        navigationItem.rightBarButtonItem = quotesView?.showAddQuoteButton
    }
}


