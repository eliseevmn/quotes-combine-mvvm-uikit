//
//  AddQuoteViewMode.swift
//  QuotesIOS
//
//  Created by MAC on 28.12.2021.
//

import Foundation
import Combine

final class AddQuoteViewModel {

    // MARK: - Properties
    var coreDataBase: QuotesCoreDataBase
    var selectedCategory: CurrentValueSubject<CategoryModel?, Never> = CurrentValueSubject(nil)
    var selectedQuote: CurrentValueSubject<QuoteModel?, Never> = CurrentValueSubject(nil)
    var isFinishScreen: (() -> Void)?
    var showAddCategoryScreen: (() -> Void)?

    // MARK: - Init
    init(coreDataBase: QuotesCoreDataBase) {
        self.coreDataBase = coreDataBase
    }

    // MARK: - Public function
    func bindingCategoryModel() {
        selectedCategory.value = selectedQuote.value?.category
    }
}
