//
//  CoordinatorFactory.swift
//  QuotesIOS
//
//  Created by MAC on 23.12.2021.
//

import Foundation

protocol CoordinatorFactoryProtocol {

    func makeQuotesCoordinator(with router: Router) -> QuotesCoordinator
}

final class CoordinatorFactory: CoordinatorFactoryProtocol {

    private let moduleFactory: ModuleFactoryProtocol

    init(moduleFactory: ModuleFactoryProtocol) {
        self.moduleFactory = moduleFactory
    }

    func makeQuotesCoordinator(with router: Router) -> QuotesCoordinator {
        return QuotesCoordinator(router: router,
                                 coordinatorFactory: self,
                                 moduleFactory: moduleFactory)
    }
}
