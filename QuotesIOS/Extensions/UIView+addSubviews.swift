//
//  UIView+Extensions.swift
//  Sarawan
//
//  Created by MAC on 22.10.2021.
//
import UIKit

extension UIView {

	func addSubviews(_ subviews: [UIView]) {
		subviews.forEach(self.addSubview)
	}

	func addSubviews(_ subviews: UIView...) {
		subviews.forEach(self.addSubview)
	}
}
