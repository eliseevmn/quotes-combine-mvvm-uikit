//
//  Modules.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation

struct QuoteModel {

    var id: String
    var author: String
    var content: String
    let timestamp: Date
    var category: CategoryModel

    init(id: String, author: String, content: String, category: CategoryModel) {
        self.id = id
        self.author = author
        self.content = content
        self.timestamp = Date()
        self.category = category
    }
}

extension QuoteModel {

    init(usingEntity entity: Quote) {
        self.id = entity.id ?? ""
        self.author = entity.author ?? ""
        self.content = entity.content ?? ""
        self.timestamp = entity.timestamp ?? Date()
        self.category = CategoryModel(usingEntity: entity.category)
    }
}
