//
//  AddCategoryViewController.swift
//  QuotesIOS
//
//  Created by MAC on 20.12.2021.
//

import UIKit
import Combine

final class AddCategoryViewController: UIViewController {

    // MARK: - Outlets
    private lazy var addCategoryView = self.view as? AddCategoryView

    // MARK: - Properties
    let viewModel: AddCategoryViewModel
    private var cancellables = Set<AnyCancellable>()
    private lazy var coreDataBase = viewModel.coreDataBase

    // MARK: - Init
    init(viewModel: AddCategoryViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycles
    override func loadView() {
        super.loadView()
        let view = AddCategoryView(viewModel: viewModel)
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupBinding()
    }
}

// MARK: - Actions
extension AddCategoryViewController {

    func setupBinding() {
        let stringPublisher = Just<String>("").eraseToAnyPublisher()
        let combines = Publishers.CombineLatest(addCategoryView?.categoryTextField.textPublisher ?? stringPublisher,
                                                viewModel.selectedColor)

        // Сохранение категории в базу данных
        addCategoryView?.saveCategoryButton.tapPublisher
            .withLatestFrom(combines) /// объединение publishers
            .filter({ $0.1 != nil || !$0.0.isEmpty}) /// фильтрация
            .map { categoryName, selectedColor in /// преобразование
                return (categoryName, selectedColor!)
            }
            .map { categoryName, selectedColor in /// преобразование
                CategoryModel(name: categoryName, legend: selectedColor)
            }
            .flatMap { [coreDataBase] in /// сохранение в базу данных
                coreDataBase.save(usingModel: $0)
            }
            .sink { error in
                print(error)
            } receiveValue: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.isFinishScreen?()
            }
            .store(in: &cancellables)

        addCategoryView?.chooseCategoryColorButton.tapPublisher
            .sink { _ in
                let picker = UIColorPickerViewController()
                picker.selectedColor = UIColor(hexString: self.viewModel.selectedColor.value)
                picker.delegate = self
                self.present(picker, animated: true, completion: nil)
            }
            .store(in: &cancellables)

        viewModel.selectedColor
            .sink(receiveValue: { [weak self] selectedColor in
                guard let self = self else { return }
                self.addCategoryView?.legendView.backgroundColor = UIColor(hexString: selectedColor)
                self.addCategoryView?.hexCodeLabel.text = selectedColor
            })
            .store(in: &cancellables)

        addCategoryView?.cancelAddCategoryButton.tapPublisher
            .sink { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.isFinishScreen?()
            }
            .store(in: &cancellables)
    }
}

// MARK: - UIColorPickerViewControllerDelegate
extension AddCategoryViewController: UIColorPickerViewControllerDelegate {

    func colorPickerViewController(_ viewController: UIColorPickerViewController,
                                   didSelect color: UIColor,
                                   continuously: Bool) {
        viewModel.selectedColor.value = color.hexStringFromColor()
        viewController.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Configure UI
extension AddCategoryViewController {

    func configureUI() {
        setupTitle(title: "Add Category")
    }
}
