//
//  QuoteDB.swift
//  QuotesIOS
//
//  Created by MAC on 19.12.2021.
//

import Foundation
import Combine

enum DatabaseError: Error {
    case saveError
    case deleteError
}

enum DatabaseResponse {
    case beginUpdates
    case endUpdates
    case insert(indexPpath: IndexPath)
    case delete(indexPpath: IndexPath)
}

protocol QuotesDBProtocol {
    var quotes: [QuoteModel] { get }
    func save(usingModel model: QuoteModel) -> Future<String, DatabaseError>
    var databaseResponseSubject: PassthroughSubject<DatabaseResponse, Never> { get }
}
