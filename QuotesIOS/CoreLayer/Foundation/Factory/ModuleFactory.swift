//
//  ModuleFactory.swift
//  QuotesIOS
//
//  Created by MAC on 23.12.2021.
//

import Foundation

protocol ModuleFactoryProtocol {

    func makeQuotesScreenModule() -> QuotesViewController
    func makeAddQuoteScreenModule() -> AddQuoteViewController
    func makeCategoriesScreenModule() -> CategoriesViewController
    func makeAddCategoryScreenModule() -> AddCategoryViewController
}

final class ModuleFactory: ModuleFactoryProtocol {

    private lazy var container = DIContainer()

    func makeQuotesScreenModule() -> QuotesViewController {
        let viewModel = QuotesViewModel(coreDataBase: container.quotesDataBase)
        let controller = QuotesViewController(viewModel: viewModel)
        return controller
    }

    func makeAddQuoteScreenModule() -> AddQuoteViewController {
        let viewModel = AddQuoteViewModel(coreDataBase: container.quotesDataBase)
        let controller = AddQuoteViewController(viewModel: viewModel)
        return controller
    }

    func makeCategoriesScreenModule() -> CategoriesViewController {
        let viewModel = CategoriesViewModel(coreDataBase: container.categoriesDataBase)
        let controller = CategoriesViewController(viewModel: viewModel)
        return controller
    }

    func makeAddCategoryScreenModule() -> AddCategoryViewController {
        let viewModel = AddCategoryViewModel(coreDataBase: container.categoriesDataBase)
        let controller = AddCategoryViewController(viewModel: viewModel)
        return controller
    }
}
